package ru.mctf

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import org.springframework.web.server.ResponseStatusException
import java.io.File

@RestController
class GuessController(
    @Value("\${backend-host:localhost:8081}")
    val backendHost: String
) {

    private val restTemplate = RestTemplate()
    private val flag = File("flag.txt").readText()

    @PostMapping("/guess")
    fun guess(@RequestBody guess: RandomGuess): GuessResult {
        val response = try {
            restTemplate.getForEntity<Long>("http://$backendHost/nextNumber?username={username}", guess.username)
        } catch (e: HttpClientErrorException) {
            e.printStackTrace()
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
        }

        val number = checkNotNull(response.body)

        return if (number == guess.number) {
            GuessResult(
                success = true,
                flag = flag
            )
        } else {
            GuessResult(
                success = false,
                number = number.toString()
            )
        }
    }

}

data class RandomGuess(
    val username: String,
    val number: Long
)

data class GuessResult(
    val success: Boolean,
    val flag: String? = null,
    val number: String? = null
)