package ru.mctf

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GuessRandomTaskFrontendApplication

fun main(args: Array<String>) {
    runApplication<GuessRandomTaskFrontendApplication>(*args)
}
