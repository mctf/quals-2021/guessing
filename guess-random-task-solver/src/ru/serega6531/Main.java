package ru.serega6531;

import java.lang.reflect.Field;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Main {

    // INSERT VALUES FROM HEAPDUMP HERE:
    private static final int remCountValue = 8;
    private static final byte[] remainderValue = {0, 0, 0, 0, 0, 0, 0, 0, -113, -46, 106, 51, 123, -62, 114, 31, 81, 77, -30, -42};
    private static final byte[] stateValue = {101, -46, -58, -127, -79, 104, 124, -59, 127, -107, -104, -33, 82, 45, -68, 116, -17, -16, 67, 51};

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException, NoSuchAlgorithmException {
        SecureRandom random = prepareRandom();
        System.out.println(random.nextLong());
    }

    private static SecureRandom prepareRandom() throws NoSuchAlgorithmException, NoSuchFieldException, IllegalAccessException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        Field field = SecureRandom.class.getDeclaredField("secureRandomSpi");
        setField(field, random, prepareSpi());
        return random;
    }

    private static sun.security.provider.SecureRandom prepareSpi() throws NoSuchFieldException, IllegalAccessException {
        sun.security.provider.SecureRandom spi = new sun.security.provider.SecureRandom();

        Field remCount = sun.security.provider.SecureRandom.class.getDeclaredField("remCount");
        Field remainder = sun.security.provider.SecureRandom.class.getDeclaredField("remainder");
        Field state = sun.security.provider.SecureRandom.class.getDeclaredField("state");

        setField(remCount, spi, remCountValue);
        setField(remainder, spi, remainderValue);
        setField(state, spi, stateValue);

        return spi;
    }

    private static void setField(Field field, Object o, Object value) throws IllegalAccessException {
        field.setAccessible(true);
        field.set(o, value);
    }

    private static void setField(Field field, Object o, int value) throws IllegalAccessException {
        field.setAccessible(true);
        field.set(o, value);
    }

}
