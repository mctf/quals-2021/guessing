package ru.mctf.guessing.controller

import org.springframework.web.bind.annotation.*
import ru.mctf.guessing.service.RandomService

@RestController
class RandomController(
    val service: RandomService
) {

    @GetMapping("/nextNumber")
    fun guess(@RequestParam username: String): Long {
        return service.getNextNumber(username)
    }

}