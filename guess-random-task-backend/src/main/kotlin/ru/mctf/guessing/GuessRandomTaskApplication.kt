package ru.mctf.guessing

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GuessRandomTaskApplication

fun main(args: Array<String>) {
	runApplication<GuessRandomTaskApplication>(*args)
}
