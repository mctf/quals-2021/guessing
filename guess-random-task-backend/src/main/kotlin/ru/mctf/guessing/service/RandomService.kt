package ru.mctf.guessing.service

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import org.springframework.stereotype.Service
import java.security.SecureRandom
import java.util.concurrent.TimeUnit

@Service
class RandomService {

    val randoms = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES) // remove old randoms
        .build(CacheLoader.from<String, SecureRandom> {
            SecureRandom.getInstance("SHA1PRNG")
        })

    fun getNextNumber(username: String): Long {
        val random = randoms.get(username)
        return random.nextLong()
    }

}